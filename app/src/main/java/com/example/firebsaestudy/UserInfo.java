package com.example.firebsaestudy;

/**
 * Created by Farzana on 11/23/2017.
 */

public class UserInfo {
   public  String name;
   public  String email;
   public String password;

    public UserInfo() {
    }

    public UserInfo(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void  setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("Name: " + name, "Email: " + email);
    }
}
