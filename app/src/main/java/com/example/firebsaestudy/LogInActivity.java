package com.example.firebsaestudy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LogInActivity extends AppCompatActivity {

    public static final String TAG = LogInActivity.class.getSimpleName();
    private Button logInButton, signUpButton;
    FirebaseAuth mAuth;
    private FirebaseDatabase database;
    DatabaseReference reference;
    DatabaseReference child;
    private EditText loginInputEmail, loginInputPassword;

    private TextInputLayout loginInputLayoutEmail, loginInputLayoutPassword;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mAuth = FirebaseAuth.getInstance();
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        database= FirebaseDatabase.getInstance();
        reference=database.getReference();
        child =reference.child("Users");
        loginInputLayoutEmail =  findViewById(R.id.login_input_layout_email);
        loginInputLayoutPassword =  findViewById(R.id.login_input_layout_password);
        progressBar =  findViewById(R.id.progressBar);

        loginInputEmail =  findViewById(R.id.login_input_email);
        loginInputPassword =  findViewById(R.id.login_input_password);

        logInButton =  findViewById(R.id.btn_login);
        signUpButton =  findViewById(R.id.btn_link_signup);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this,ProfileActivity.class));
            }
        });
    }

    private void submitForm() {
        final String email = loginInputEmail.getText().toString().trim();
        final String password = loginInputPassword.getText().toString().trim();

        if(!checkMail()){
            return;
        }
        if(!checkPassword()){
            return;
        }
        loginInputLayoutEmail.setErrorEnabled(false);
        loginInputLayoutPassword.setErrorEnabled(false);

        progressBar.setVisibility(View.VISIBLE);
        //authenticate user in here
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //if the sign in fail a toast will appear
                        //the auth state listener won't be notified
                        //signed in user can be handled in the  listener
                        progressBar.setVisibility(View.GONE);
                        if(!task.isSuccessful()){
                            //there was an error
                            Toast.makeText(LogInActivity.this,getString(R.string.auth_failed),Toast.LENGTH_LONG).show();
                        }
                        else {


                            child.addValueEventListener(new ValueEventListener() {

                                String name;
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot shot:dataSnapshot.getChildren()) {

                                        UserInfo info = shot.getValue(UserInfo.class);
                                       if(email.equals(info.getEmail())){
                                           name = info.getName();
                                           SharedPreferences preferences = LogInActivity.this.getSharedPreferences("user", Context.MODE_PRIVATE);
                                           SharedPreferences.Editor editor = preferences.edit();
                                           editor.putString("name",name);
                                           editor.putString("email",email);
                                           editor.commit();
                                           Log.i("sender ","is "+name);
                                       }
                                    }

                                        UserInfo infos = new UserInfo();
                                        infos.setName(name);
                                        infos.setEmail(email);
                                        infos.setPassword(password);
                                        Intent intent = new Intent(LogInActivity.this,UserActivity.class);
                                        intent.putExtra("name",name);

                                        //  startActivity(new Intent(LogInActivity.this,UserActivity.class));
                                        startActivity(intent);
                                        finish();


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            //   Log.i("email","is "+info.getEmail());

                        }
                    }
                });
    }

    private boolean checkMail() {
        String email = loginInputEmail.getText().toString().trim();
        if (email.isEmpty() || !isEmailValid(email)) {

            loginInputLayoutEmail.setErrorEnabled(true);
            loginInputLayoutEmail.setError(getString(R.string.err_msg_email));
            loginInputEmail.setError(getString(R.string.err_msg_required));
            requestFocus(loginInputEmail);
            return false;
        }
        loginInputLayoutEmail.setErrorEnabled(false);
        return true;
    }

    private boolean checkPassword() {
        String password = loginInputPassword.getText().toString().trim();
        if (password.isEmpty() || !isPasswordValid(password)) {

            loginInputLayoutPassword.setError(getString(R.string.err_msg_password));
            loginInputPassword.setError(getString(R.string.err_msg_required));
            requestFocus(loginInputPassword);
            return false;
        }
        loginInputLayoutPassword.setErrorEnabled(false);
        return true;
    }
    private static boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isPasswordValid(String password){
        return (password.length() >= 6);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}
