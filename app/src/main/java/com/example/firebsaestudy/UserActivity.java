package com.example.firebsaestudy;

import android.*;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends AppCompatActivity {

    android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth mAuth;
    //  TextView userText;
    private FirebaseAuth.AuthStateListener authStateListener;

    ListView recyclerView;
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference child;
    List<User>  users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.user_list);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        child = myRef.child("Users");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(Intent.ACTION_MAIN);
              intent.addCategory(Intent.CATEGORY_HOME);
              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(intent);
            }
        });

        // userText = findViewById(R.id.user_name);

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser currentUser = mAuth.getCurrentUser();
                if(currentUser == null){
                    startActivity(new Intent(UserActivity.this,StartActivity.class));
                    finish();
                }
                else{
                    //   userText.setText("Hello "+currentUser.getEmail());
                    child.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            users =  new ArrayList<>();

                            for (DataSnapshot shot: dataSnapshot.getChildren()) {
                                User info  = shot.getValue(User.class);
                                User user = new User();
                                String phone = info.getPhone();
                                user.setPhone(phone);
                                users.add(user);

                                Log.i("users",user.toString());


                            }


                            recyclerView.setAdapter(new CustomAdapter(getApplicationContext(),R.layout.single_row,users));
                            recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                    String reciver = users.get(position).getPhone();
                                    //  Log.i("receiver",reciver.getReceiver());
                                    SharedPreferences preferences = UserActivity.this.getSharedPreferences("receiver",Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor =  preferences.edit();
                                    editor.putString("receiver",reciver);
                                    editor.commit();

                                    //   Log.i("name",name);
                                    startActivity(new Intent(UserActivity.this,ChatActivity.class));
                                }
                            });

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        };

    }

    class CustomAdapter extends ArrayAdapter<User> {

        List<User> usersList = new ArrayList<>();
        TextView phoneTv;
        LinearLayout layout;
        public CustomAdapter(@NonNull Context context, int resource,List<User> usersList){
            super(context,resource,usersList);
            this.usersList =usersList;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                convertView = LayoutInflater.from(UserActivity.this).inflate(R.layout.single_row,parent,false);
                phoneTv = convertView.findViewById(R.id.phone_text_view);
            }

            phoneTv.setText(usersList.get(position).getPhone());


            return convertView;
        }

    }
      /* public CustomAdapter(List<UserInfo> users){
            this.usersList = users;
        }
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(UserActivity.this).inflate(R.layout.single_row,parent,false);
            return new MyViewHolder(view);
        }
 */
      /*  @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.nameTv.setText(usersList.get(position).getName());
            holder.emailTv.setText(usersList.get(position).getEmail());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*Intent intent = new Intent(UserActivity.this,ChatActivity.class);
                   intent.putExtra("name",users.get(position).getName());
                   intent.putExtra("email",users.get(position).getEmail());
                   startActivity(intent);

                }
            });
        }
 */


      /*  class MyViewHolder extends RecyclerView.ViewHolder{
            TextView nameTv,emailTv;
            CardView cardView;
            public MyViewHolder(View itemView) {
                super(itemView);
                nameTv = itemView.findViewById(R.id.name_text_view);
                emailTv = itemView.findViewById(R.id.email_text_view);
                cardView = itemView.findViewById(R.id.card_view);
            }
        }
    } */


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(authStateListener!=null){
            mAuth.removeAuthStateListener(authStateListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.sign_out){
            mAuth.signOut();
        }
        return true;
    }

}