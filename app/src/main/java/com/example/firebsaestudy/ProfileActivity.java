package com.example.firebsaestudy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfileActivity extends AppCompatActivity {

    TextInputLayout signUpEmailInputLayout, signUpPasswordInputLayout, signUpNameInputLayout;
    EditText signUpEmailEditText, signUpPasswordEditText, signUpNameEditText;
    Button signUpButton;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();
        signUpEmailInputLayout = findViewById(R.id.sign_up_input_layout_email);

        signUpNameInputLayout = findViewById(R.id.sign_up_input_layout_name);
        signUpEmailEditText = findViewById(R.id.sign_up_input_email);

        signUpNameEditText = findViewById(R.id.sign_up_input_name);
        signUpButton =findViewById(R.id.save_btn);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

    }

    private void submitForm() {
        final String email = signUpEmailEditText
                .getText().toString().trim();
        final String password = signUpPasswordEditText
                .getText().toString().trim();
        final String name = signUpNameEditText
                .getText().toString().trim();

        if (!checkEmail()) {
            return;
        }
        if (!checkPassword()) {
            return;
        }
        signUpEmailInputLayout.setErrorEnabled(false);
        signUpPasswordInputLayout.setErrorEnabled(false);



        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(ProfileActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                       // progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {

                        }
                    }

                });
        Toast.makeText(getApplicationContext(), "Your are successfully registered",Toast.LENGTH_LONG).show();
    }

    private boolean checkPassword() {
        String password = signUpPasswordEditText.getText().toString().trim();
        if(password.isEmpty() || !isPasswordValid(password)){

            signUpPasswordInputLayout.setError(getString(R.string.err_msg_password));
            signUpPasswordEditText.setError(getString(R.string.err_msg_required));
            requestFocus(signUpPasswordEditText);
            return false;
        }
        signUpPasswordInputLayout.setErrorEnabled(false);
        return true;
    }

    private boolean checkEmail() {
        String email = signUpEmailEditText.getText().toString().trim();
        if(email.isEmpty() || !isEmailValid(email)){

         signUpEmailInputLayout.setErrorEnabled(true);
         signUpEmailInputLayout.setError(getString(R.string.err_msg_email));
         requestFocus(signUpEmailEditText);
         return false;
        }
        signUpEmailInputLayout.setErrorEnabled(false);
        return true;
    }

    private static boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private static boolean isPasswordValid(String password){
        return (password.length()>= 6);
    }
    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
