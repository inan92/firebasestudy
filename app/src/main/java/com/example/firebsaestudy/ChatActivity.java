package com.example.firebsaestudy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    LinearLayout layout;
    ScrollView scrollView;
    FloatingActionButton sendBtn;
    ImageButton sendImageBtn;
    EditText edt;
    DatabaseReference reference;
    FirebaseDatabase database;
    public static final int RC_SIGN_IN = 1;
    android.support.v7.widget.Toolbar toolbar;
    DatabaseReference childReference;
    DatabaseReference childReference2;
    private static final int RC_PHOTO_PICKER =  2;
    private StorageReference mStorageRef;
    private StorageReference nStorageRef;
    private FirebaseStorage mStorage;
    private MediaRecorder mRecorder;
    Button recordBtn;
    String reciver;
    String name;
    private String fileName = null;
    private int currentFormat =0;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences preferences = ChatActivity.this.getSharedPreferences("user", Context.MODE_PRIVATE);
        name = preferences.getString("phone",null);
        SharedPreferences preferences1 = ChatActivity.this.getSharedPreferences("receiver",Context.MODE_PRIVATE);
        reciver=preferences1.getString("receiver",null);
        mStorage = FirebaseStorage.getInstance();
        getSupportActionBar().setTitle(reciver);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dialog = new ProgressDialog(this);
        layout = findViewById(R.id.layout1);
        scrollView = findViewById(R.id.scroll);
        sendBtn = findViewById(R.id.send_message_btn);
        edt = findViewById(R.id.message_area);
        sendImageBtn = findViewById(R.id.photoPickerButton);
        recordBtn = findViewById(R.id.record_btn);
        database = FirebaseDatabase.getInstance();
        reference = database.getReference();
        final Message message = new Message();
        mStorageRef = mStorage.getReference().child("chat_photos");
        message.setReceiver(reciver);

        //the first reference node in firebase where message is saved for the sender
        childReference = reference.child(name +"_"+ message.getReceiver());
        // second node in firebase where message is saved for the receiver
        childReference2 =  reference.child(message.getReceiver()+"_" + name);


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String messageText = edt.getText().toString();

                if (!messageText.equals("")) {

                    Message message = new Message();
                    message.setSender(name);
                    message.setMessage(messageText);
                    //setting value for the 1st node in firebase database
                    childReference.push().setValue(message);
                    //setting value for the 2nd node in firebase database
                    childReference2.push().setValue(message);
                    edt.setText("");
                }
            }
        });
        sendImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

                startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);
            }
        });

        recordBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(ChatActivity.this,"Start Recording",Toast.LENGTH_SHORT).show();
                        startRecording();
                        break;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(ChatActivity.this,"Stop Recording",Toast.LENGTH_SHORT).show();
                        stopRecording();
                        break;
                }
                return false;
            }
        });
        childReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //from the  1st node the message is retrieved to see
                Message userMessage = dataSnapshot.getValue(Message.class);
                String message2 = userMessage.getMessage();
                String user = userMessage.getSender();
                String uri = userMessage.getPhotoUrl();
                String audio = userMessage.getAudioUri();
                boolean isPhoto = uri != null;
                boolean isAudio = audio !=null;
                if (user.equals(name) && isPhoto && !isAudio) {
                    addPictureMessage(userMessage, 1);
                } else if(!isPhoto && user.equals(name) &&!isAudio) {
                    addMessageBox( message2, 1);
                } else if(!isPhoto && !user.equals(name) && !isAudio){
                    addMessageBox(message2,2);
                } else if(isPhoto && !user.equals(name) && !isAudio){
                    addPictureMessage(userMessage, 2);
                } else if(!isPhoto && user.equals(name) && isAudio){
                    addAudioMessageBox(audio,1);
                } else if(!isPhoto && !user.equals(name) && isAudio){
                    addAudioMessageBox(audio,2);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(getFileName());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.i("error", "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        uploadAudio();
    }
    private String getFileName(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);
        fileName = file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat];
        nStorageRef = mStorage.getReference().child("audio_messages").child(fileName);

        return fileName;
    }


    private void uploadAudio() {
        dialog.setMessage("Uploading audio");
        dialog.show();
        final File file = new File(fileName);
        Uri uri =  Uri.fromFile(file);


        Log.i("storage",nStorageRef.getName());
        nStorageRef.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                dialog.setMessage("Finished");
                dialog.show();
                dialog.dismiss();
                if(file.exists()){
                    file.delete();
                }
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Message friendlyMessage = new Message(name, reciver,null,null, downloadUrl.toString());
                Log.i("audio",downloadUrl.toString());
                childReference.push().setValue(friendlyMessage);
                childReference2.push().setValue(friendlyMessage);
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK) {

            Uri selectImageURI = data.getData();
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(
                        selectImageURI);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bmp = BitmapFactory.decodeStream(imageStream);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            try {
                stream.close();

            } catch (IOException e) {

                e.printStackTrace();
            }
            StorageReference photoRef = mStorageRef.child(selectImageURI.getLastPathSegment());
            photoRef.putBytes(byteArray).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    Message friendlyMessage = new Message(name, reciver,null, downloadUrl.toString());
                    Log.i("picture",downloadUrl.toString());
                    childReference.push().setValue(friendlyMessage);
                     childReference2.push().setValue(friendlyMessage);

                }
            });
        }
    }

    private void addMessageBox(String s, int i) {
        //creating the textview to show the message
        TextView textView = new TextView(ChatActivity.this);
        textView.setText(s);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


        textView.setTextColor(Color.WHITE);

        lp.setMargins(10,10,10,10);


        lp.width=260;
        lp.weight=1.0f;
        if(i==1){
            lp.gravity= Gravity.RIGHT;
            textView.setBackgroundResource(R.drawable.rounded_corner_one);
        }
        else{
            lp.gravity=Gravity.LEFT;
            textView.setBackgroundResource(R.drawable.rounded_corner_one);
        }
        textView.setLayoutParams(lp);
        layout.addView(textView);
       scrollView.post(new Runnable() {
           @Override
           public void run() {
               scrollView.fullScroll(View.FOCUS_DOWN);
           }
       });
    }
    private void addPictureMessage(Message message,int i){
        ImageView image = new ImageView(ChatActivity.this);
        String message2 = message.getPhotoUrl();

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,10,10,10);
        lp.width=260;
        lp.weight=1.0f;
        if(i==1){
            lp.gravity= Gravity.RIGHT;
            Picasso.with(ChatActivity.this).load(message2).into(image);
            Log.i("pic","is "+message.getPhotoUrl());
            image.setVisibility(View.VISIBLE);
        }
        else{
            lp.gravity=Gravity.LEFT;
            Picasso.with(ChatActivity.this).load(message2).into(image);
            image.setVisibility(View.VISIBLE);
        }
        image.setLayoutParams(lp);
        layout.addView(image);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
    private void addAudioMessageBox(String s, int i){

        LinearLayout linearLayout =  new LinearLayout(ChatActivity.this);
        final SeekBar seekBar = new SeekBar(ChatActivity.this);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        Log.i("uri",s);
        Button btn = new Button(ChatActivity.this);

        final MediaPlayer player;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,10,10,10);

        player=new MediaPlayer();
        Uri uri = Uri.parse(s);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            player.setDataSource(ChatActivity.this,uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
           player.prepare();
        } catch (IllegalStateException e) {
           e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(player.isPlaying()){
                    player.pause();
                }
                else {
                    player.start();
                    seekBar.setMax(100);
                    seekBar.setProgress(player.getCurrentPosition());
                }
            }
        });

        if(i==1){
            lp.gravity=Gravity.RIGHT;
            linearLayout.setBackgroundResource(R.drawable.rounded_corner_one);
        }
        else {
            lp.gravity = Gravity.LEFT;
            linearLayout.setBackgroundResource(R.drawable.rounded_corner_one);
        }
        linearLayout.setLayoutParams(lp);
        linearLayout.addView(seekBar);
        linearLayout.addView(btn);
        seekBar.setLayoutParams(lp);
        btn.setLayoutParams(lp);

        layout.addView(linearLayout);
    }
}
