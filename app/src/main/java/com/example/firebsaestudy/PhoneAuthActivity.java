package com.example.firebsaestudy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.TimeUnit;

public class PhoneAuthActivity extends AppCompatActivity {

    EditText phoneNumberEdt,verificationCode;
    Button sendButton,signInButton;
    String verficationId;
    private FirebaseAuth mAuth;
    android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        phoneNumberEdt = findViewById(R.id.phone_nmbr_edt);
        verificationCode = findViewById(R.id.verification_code_edt);
        sendButton = findViewById(R.id.send_btn);
        signInButton = findViewById(R.id.sign_in_btn_main);

        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(mAuth.getCurrentUser() != null){
                    Toast.makeText(PhoneAuthActivity.this,"You are logged in "+mAuth.getCurrentUser().getProviderId()
                            ,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PhoneAuthActivity.this,UserActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = phoneNumberEdt.getText().toString();

                if(!TextUtils.isEmpty(phoneNumber)){
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phoneNumber, 60, TimeUnit.SECONDS, PhoneAuthActivity.this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                @Override
                                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                                    signInWithPhoneCredential(phoneAuthCredential);
                                }

                                @Override
                                public void onVerificationFailed(FirebaseException e) {
                                    Toast.makeText(PhoneAuthActivity.this,"Verication failed "+e.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                    super.onCodeSent(s, forceResendingToken);
                                    verficationId = s;
                                }

                                @Override
                                public void onCodeAutoRetrievalTimeOut(String s) {
                                    super.onCodeAutoRetrievalTimeOut(s);
                                }
                            }
                    );
                }
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = verificationCode.getText().toString();
                if(TextUtils.isEmpty(code)){
                    return;
                }
                signInWithPhoneCredential(PhoneAuthProvider.getCredential(verficationId,code));
            }
        });
    }

    private void signInWithPhoneCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(PhoneAuthActivity.this,"Signed in Success",Toast.LENGTH_SHORT).show();
                            User user = new User(phoneNumberEdt.getText().toString());
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference reference = database.getReference("Users");
                            DatabaseReference childRef =  reference.child(user.getPhone());
                            //reference.child(user.getUid()).setValue(info);
                            SharedPreferences preferences = PhoneAuthActivity.this.getSharedPreferences("user", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("phone",user.getPhone());
                            editor.commit();
                            childRef.setValue(user);
                        }
                        else {
                            Toast.makeText(PhoneAuthActivity.this,"Not Signed in",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }
 }

