package com.example.firebsaestudy;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Farzana on 11/27/2017.
 */

public class StartActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    Button enterButton;

    android.support.v7.widget.Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        mAuth = FirebaseAuth.getInstance();
        enterButton = findViewById(R.id.sign_in_phn);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    enterButton.setBackground(getResources().getDrawable(R.drawable.button_choose));
                }
                enterButton.setTextColor(Color.WHITE);
                startActivity(new Intent(StartActivity.this,PhoneAuthActivity.class));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser()!=null){
            startActivity(new Intent(StartActivity.this,UserActivity.class));
        }
    }
}
