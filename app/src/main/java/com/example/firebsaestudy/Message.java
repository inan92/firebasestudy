package com.example.firebsaestudy;

/**
 * Created by Farzana on 11/25/2017.
 */

public class Message {
    public  String sender;
    public  String receiver;
    public  String message;
    private String photoUrl;
    private String audioUri;

    public Message() {
    }

    public Message(String sender, String receiver, String message, String photoUrl) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.photoUrl = photoUrl;
    }

    public String getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(String audioUri) {
        this.audioUri = audioUri;
    }

    public Message(String sender, String receiver, String message, String photoUrl, String audioUri) {

        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.photoUrl = photoUrl;
        this.audioUri = audioUri;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public  String getSender() {
        return sender;
    }

    public  void setSender(String sender) {
        this.sender = sender;
    }

    public  String getReceiver() {
        return receiver;
    }

    public  void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public  String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
